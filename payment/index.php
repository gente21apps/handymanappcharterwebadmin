<?php

include 'conn.php';

//let's check if this offer exists, otherwise bail
if(isset($_GET['uid'])){
    $sql = "SELECT place.*, users.name AS prov_name, users.email AS prov_email FROM place, users";
    $sql .= " WHERE uid = '".$_GET['uid']."' AND place.user_id = users.id";

    $result = $mysqli->query($sql);
    $oferta = $result->fetch_assoc();

    if ($result->num_rows == 0) {
        die("Esta oferta no se encontr&oacute; o el proveedor no existe :O");
    } else {
        //let's check if it's not expired
        $expdate = strtotime($oferta['fecha_vencimiento']);
        $today = strtotime('today');
        if($today > $expdate){
            die("Esta oferta ha expirado :O");
        }
    }
} else {
    die("Esta oferta no existe :O");
}
        

?>
<html>
    <head>
        <title>Charter Go</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
        <link href='custom.css' rel='stylesheet' type='text/css'>

        <!-- para el datepicker -->
        <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
        <script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">

                    <h1 style="margin-bottom: 20px;">Reservación de viaje <br><a href="#"><?php echo $oferta['name']; ?></a></h1>

                    <p class="lead">
                        Por favor llene la forma de contacto con los datos que se solicitan
                    </p>


                    <form id="contact-form" method="post" action="contact.php" role="form">

                        <div class="messages">
                            <input type="hidden" name="viaje" value="<?php echo $oferta['name']; ?>">
                            <input type="hidden" name="viaje_uid" value="<?php echo $oferta['uid']; ?>">
                            <input type="hidden" name="condiciones" value="<?php echo $oferta['condiciones']; ?>">
                            <input type="hidden" name="precio_por_persona" value="<?php echo $oferta['precio_por_persona']; ?>">
                            <input type="hidden" name="precio_divisa" value="<?php echo $oferta['precio_divisa']; ?>">
                        </div>

                        <div class="controls">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_name">Nombre completo *</label>
                                        <input id="form_name" type="text" name="name" class="form-control" placeholder="Nombre completo *" required="required" data-error="El nombre es requerido">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_lastname">Dirección *</label>
                                        <input id="form_direccion" type="text" name="direccion" class="form-control" placeholder="Dirección *" required="required" data-error="La dirección es requerida">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_name">Ciudad y estado *</label>
                                        <input id="form_city" type="text" name="city" class="form-control" placeholder="Ciudad y estado en donde reside *" required="required" data-error="El nombre es requerido">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_lastname">Código postal *</label>
                                        <input id="form_cp" type="text" name="cp" class="form-control" placeholder="00000 *" required="required" data-error="La dirección es requerida">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_email">Email *</label>
                                        <input id="form_email" type="email" name="email" class="form-control" placeholder="Email *" required="required" data-error="Se requiere un email válido">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_phone">Teléfono *</label>
                                        <input id="form_phone" type="tel" name="phone" class="form-control" placeholder="Número de teléfono *" required="required" data-error="Se requiere un número de teléfono válido">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_email">¿Cuántos adultos viajan?</label>
                                        <br>
                                        <select id="adultos" name="num_adultos" class="form-control">
                                            <?php 
                                              for ($i=1; $i <= $oferta['lugares']; $i++) { 
                                                  echo '<option value='.$i.'>'.$i.'</option>';
                                              }
                                            ?>                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_phone">¿Cuántos niños viajan?</label>
                                        <br>
                                        <!-- 
                                            - Aguas aquí que si solo hay un lugar disponible no debería haber lugar para niños 
                                            - Obvio si ya se seleccionó un número de adultos hay que reducir el número de niños
                                        -->
                                        <select id="ninos" name="num_ninos" class="form-control">
                                            <?php 
                                              for ($i=0; $i <= ($oferta['lugares'] - 1); $i++) { 
                                                  echo '<option value='.$i.'>'.$i.'</option>';
                                              }
                                            ?>                                 
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <!-- METODO DE PAGO -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="form_email">Método de pago (próximamente pago por OXXO)</label>
                                        <div class="radio">
                                          <label><input type="radio" name="optradio" value="oxxo" disabled>Pago en efectivo en OXXO</label>
                                        </div>
                                        <div class="radio">
                                          <label><input type="radio" name="optradio" value="tarjeta" checked>Pago con tarjeta de crédito o débito (todas las tarjetas)</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="form_message">Por favor lea las condiciones del servicio antes de continuar</label>
                                        <br>
                                        <small>
                                            <?php  echo $oferta['condiciones']; ?>
                                        </small>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="form_message">¿Tiene algún comentario, duda o pregunta?</label>
                                        <textarea id="form_message" name="message" class="form-control" placeholder="Gracias por sus comentarios" rows="4"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-success btn-send" value="Reservar ahora">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="text-muted"><strong>*</strong> Estos campos son requeridos.
                                        <br><br>
                                    Servicio proporcionado por Brands México, una división de <a href="http://gente21.com" target="_blank">Gente21</a>.</p>
                                </div>
                            </div>
                        </div>

                    </form>

                </div><!-- /.8 -->

            </div> <!-- /.row-->

        </div> <!-- /.container-->

        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    </body>
</html>
